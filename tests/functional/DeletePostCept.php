<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('delete a post');

$I->amOnPage('/');
$I->click('New Post');
$I->seeCurrentUrlEquals('/post/create');

$I->fillField('Title', 'Delete this post please');
$I->fillField('Content', 'I want to delete this post as long its not needed');
$I->click('Save');
$I->click('View Post');

$I->see('Delete this post please');
$I->see('I want to delete this post as long its not needed');
$I->click('Delete Post');

$I->seeCurrentUrlEquals('');
$I->see('Post Deleted');
$I->dontSee('Delete this post please');
