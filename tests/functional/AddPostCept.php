<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('create a post');

$I->amOnPage('/');
$I->click('New Post');
$I->seeCurrentUrlEquals('/post/create');

$I->fillField('Title', 'Another blog post');
$I->fillField('Content', 'This is the part where we rock it');
$I->click('Save');
$I->click('View Post');

$I->see('Another blog post');
$I->see('This is the part where we rock it');