<?php
$I = new FunctionalTester($scenario);
$I->am('a guest');

$I->amOnRoute('user.create');

$I->fillField('Username', 'brucew');
$I->fillField('Email', 'brucewayne@wayneenterprises.com');
$I->fillField('Full Name', 'Bruce Wayne');
$I->fillField('Password', 'batman');
$I->fillField('Confirm Password', 'batman');
$I->click('Create');

$I->seeCurrentUrlEquals('');
$I->see('User Created');

//$I = new FunctionalTester($scenario);
//$I->wantTo('delete a post');
//
//$I->click('New Post');
//$I->seeCurrentUrlEquals('/post/create');
//
//$I->fillField('Title', 'Delete this post please');
//$I->fillField('Content', 'I want to delete this post as long its not needed');
//$I->click('Save');
//$I->click('View Post');
//
//$I->see('Delete this post please');
//$I->see('I want to delete this post as long its not needed');
//$I->click('Delete Post');
//
//$I->seeCurrentUrlEquals('');
//$I->see('Post Deleted');
//$I->dontSee('Delete this post please');
