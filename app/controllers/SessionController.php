<?php

class SessionController extends \BaseController {

	/**
	 * The login form
	 * GET /session/create
	 *
	 * @return Response
	 */
	public function create()
	{
            Return View::make('session.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /session
	 *
	 * @return Response
	 */
	public function store()
	{
            $input = Input::all();
            $validator =  Validator::make($input, User::$signinRules);
            if ($validator->failed()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }
            
            $attempt = Auth::attempt([
               'username' => $input['username'],
               'password' => $input['password']
            ]);
            
            //dd($attempt);
            if ($attempt){
                return Redirect::intended('')->with('flash_message', 'Welcome back');
            }
            
            return Redirect::back()->with('flash_message' ,'Invalid Information')->withErrors($validator)->withInput();;
	}

	/**
	 * Display the specified resource.
	 * GET /session/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /session/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /session/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /session/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id = null)
	{
            Auth::logout();
            return Redirect::route('home')->with('You have logged out');

	}

}