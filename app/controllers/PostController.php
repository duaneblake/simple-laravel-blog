<?php

class PostController extends \BaseController {

        /**
         * Only allow index and show to be shown when not logged in
         */
        function __construct() {
            $this->beforeFilter('auth',  ['except' => ['index', 'show']]);
        }
    
	/**
	 * Display a listing of the resource.
	 * GET /post
	 *
	 * @return Response
	 */
	public function index()
	{
            
            $posts = Post::orderBy('created_at', 'DESC')->get();
            return View::make('posts.index', compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /post/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
              return View::make('posts.create');
	}

	/**
	 * Store a the post for the page
	 * POST /post
	 *
	 * @return Response
	 */
	public function store()
	{
            $input      = Input::all();
            $validator  =  Validator::make($input, Post::$rules);
            
            if ($validator->failed()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }
            
            $createdPost = Post::create([
                'title'     => $input['title'],
                'body'      => $input['body'],
                'published' => 0,
                'user_id'   => 1
            ]);
            
            return Redirect::route('post.edit', $createdPost->id);
	}

	/**
	 * Display the individual post
	 * GET /post/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            $post = Post::findOrFail($id);
            return View::make('posts.show', compact('post'));
	}

	/**
	 * Edit the form
	 * GET /post/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $post = Post::find($id);
            Return View::make('posts.edit', compact('post'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /post/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $post = Post::findOrfail($id);
            $input = Input::all();
            $validator = Validator::make($input, Post::$rules);
            
            if ($validator->failed()) {
                return Redirect::back()->withInput();
            }

            $post->title         = $input['title'];
            $post->body          = $input['body'];
            $post->published     = $input['published'];
            $post->user_id       = 1;
            $post->save();
            
            return Redirect::back();
	}

	/**
	 * Remove the post
	 * DELETE /post/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
            Post::findOrFail($id)->delete();
            return Redirect::route('home')->with('flash_message', 'Post Deleted');
	}

}