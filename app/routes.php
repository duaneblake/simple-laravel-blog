<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'home', 
    'uses' =>  'PostController@index'
]);

Route::get('/login',[
    'as'    => 'login_path',
    'uses'  => 'SessionController@create'
]);

Route::get('/logout', [
    'as'    => 'logout_path',
    'uses'  => 'SessionController@destroy'
]);

Route::resource('session', 'SessionController' );
Route::resource('post', 'PostController' );

Route::group(['before' => 'auth'], function(){

    Route::resource('user', 'UserController' );
    
});
