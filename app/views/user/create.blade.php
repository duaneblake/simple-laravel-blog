@extends('layouts.default')
@section('content')
    {{ Form::open(['route' => 'user.store']) }}
            <div class="form-group-block">
                {{ Form::label('username', 'Username') }}
                {{ Form::text('username', null, ['class' => 'formTextField  formTextFieldMedium']) }}
            </div><!-- .form-group -->
            <div class="form-group-block">
                {{ Form::label('email', 'Email') }}
                {{ Form::email('email', null, ['class' => 'formTextField  formTextFieldMedium']) }}
            </div><!-- .form-group -->
            <div class="form-group-block">
                {{ Form::label('fullname', 'Full Name') }}
                {{ Form::text('fullname', null, ['class' => 'formTextField  formTextFieldMedium']) }}
            </div><!-- .form-group -->
            <div class="form-group-block">
                {{ Form::label('password', 'Password') }}
                {{ Form::password('password', null, ['class' => 'formTextField  formTextFieldMedium']) }}
            </div><!-- .form-group -->
            <div class="form-group-block">
                {{ Form::label('password_confirmation', 'Confirm Password') }}
                {{ Form::password('password_confirmation', null, ['class' => 'formTextField  formTextFieldMedium']) }}
            </div><!-- .form-group -->
            <div class="form-group">
                {{ Form::submit('Create', ['class' => 'btn btn-default float-right']) }}
            </div><!-- .form-group -->   
    {{ Form::close() }}
@stop