@extends('layouts.default')

@section('content')
<h1>{{ $post->title }}</h1>
<div>{{ Markdown::string($post->body) }}</div>
@if (Auth::check())
    {{ link_to_action('PostController@edit', 'Edit Post', $post->id) }}
    {{ Form::open(['route' => ['post.destroy', $post->id], 'method' => 'DELETE']) }}
        {{ Form::submit('Delete Post') }}
    {{ Form::close() }}
@endif
@stop