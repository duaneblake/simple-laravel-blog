@extends('layouts.default')

@section('content')
{{ Form::model($post, ['method' => 'PATCH', 'route' => ['post.update', $post->id]]) }}
    {{ Form::hidden('published', $post->published) }}
    @include('posts.shared.form')
{{ Form::close() }}

{{ link_to_action('PostController@show', 'View Post', $post->id) }}
{{ Form::open(['route' => ['post.destroy', $post->id], 'method' => 'DELETE']) }}
    {{ Form::submit('Delete Post') }}
{{ Form::close() }}

@stop