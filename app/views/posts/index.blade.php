@extends('layouts.default')

@section('content')
<ul>
@foreach ($posts as $post)
<li>
    <h2>{{ link_to_route('post.show', $post->title, $post->id) }}</h2>
    <small>{{ $post->created_at }}</small>
    <div>{{ Markdown::string($post->body) }}</div>
    @if (Auth::check())
    <p>{{ link_to_route('post.edit', 'Edit Post', $post->id) }}</p>
    @endif
</li>
@endforeach 
</ul>
@stop