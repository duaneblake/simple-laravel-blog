            <div class="form-group-block">
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title', null, ['class' => 'formTextField  formTextFieldMedium']) }}
            </div><!-- .form-group -->
            <div class="form-group-block">
                {{ Form::label('body', 'Content') }}
                {{ Form::textarea('body', null, ['class' => 'formTextField  formTextFieldMedium']) }}
            </div><!-- .form-group -->
            <div class="form-group">
                {{ Form::submit('Save', ['class' => 'btn btn-default float-right']) }}
            </div><!-- .form-group -->            