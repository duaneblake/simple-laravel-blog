<!DOCTYPE HTML>
<html lang="en">
    <head>
	<title>title</title>
	<meta charset="UTF-8">
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css' />
        <style type="text/css" media="screen">
            body{font-family: 'Lato', sans-serif; color:#777;}
            h1, a{color:#333;}
        </style>
    </head>
    <body>
        <div id="container">
            @include('common.header')
            @include('common/messages')
            @yield('content')
        </div><!-- #container -->
    </body>
</html>
