@extends('layouts.default')

@section('content')
<div>
    {{ Form::open(['route' => 'session.store']) }}
        <div class="form-group-block">
            {{ Form::label('username', 'Username') }}
            {{ Form::text('username', null, ['class' => 'formTextField  formTextFieldMedium']) }}
        </div><!-- .form-group -->
        <div class="form-group-block">
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', null, ['class' => 'formTextField  formTextFieldMedium']) }}
        </div><!-- .form-group -->
        <div class="form-group">
            {{ Form::submit('Login', ['class' => 'btn btn-default float-right']) }}
        </div><!-- .form-group -->                
    {{ Form::close() }}
</div>
@stop