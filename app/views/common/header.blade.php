<div id="header">
    {{ link_to_route('home', 'Simple Blog', null, ['class' => 'header__logo']) }}
    @if (Auth::check())
    {{ link_to_route('post.create', 'New Post') }}
        {{ link_to_route('logout_path', 'Logout') }}
    @endif
</div>  