            @if (Session::get('flash_message'))
                <div class="container flash-message">
                        <div class="bs-callout bs-callout-warning">	
                            <p>{{ Session::get('flash_message')}}</p>
                            @if ($errors)
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif						
                        </div><!-- .bs-callout bs-callout-warning -->
                </div><!-- .flashmessage -->
            @endif