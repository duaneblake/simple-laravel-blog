<?php
use Carbon\Carbon;

class Post extends \Eloquent {
    /**
     *  Protected fields from being filled in
     * @var type Array
     */
    protected $fillable = ['published', 'user_id', 'body', 'title'];

    /**
     * Validation for a post type
     * @var array
     */
    public static $rules = [
        'title'     =>      'required|min:5',
        'body'      =>      'required|min:5',
        'user_id'    =>      'required',
        
    ];

    /**
     * Changes how the date is outputed
     * https://coderwall.com/p/ms-a1g/change-created_at-display-format-globally-in-laravel-4
     * @param type $attr
     * @return Date formated at Month Day Yeat
     */
    public  function getCreatedAtAttribute($attr){
        return Carbon::parse($attr)->format('F jS Y');
    }
}