<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

        /**
        *  Protected fields from being filled in
        * @var type Array
        */
        protected $fillable = ['username', 'email', 'password', 'fullname'];
        
        /**
         * Validate creating a user
         * @var array
         */
        public static $rules = [
            'username'  => 'required|min:3|unique:users',
            'email'  => 'required|unique:users|email',
            'password'  => 'required|min:5|confirmed'
        ];
        
        public static $signinRules = [
            'username'  => 'required|min:3',
            'password'  => 'required'
        ];

}
